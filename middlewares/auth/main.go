package auth

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Required Check for auth
func Required() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		user := session.Get("user")
		if user == nil {
			// You'd normally redirect to login page
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Invalid session token"})
			return
		}
		// Continue down the chain to handler etc
		c.Next()
	}
}
