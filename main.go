package main

import (
	"pmr-gin-example/middlewares/auth"
	"pmr-gin-example/routes/authenticated"
	"pmr-gin-example/routes/public"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	public := r.Group("/")
	publicroutes.Bootstrap(public)

	private := r.Group("/private")
	private.Use(auth.Required())
	authenticatedroutes.Bootstrap(private)

	r.Run(":8080")
}
