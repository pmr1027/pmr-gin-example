package publicroutes

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func login(c *gin.Context) {
	session := sessions.Default(c)
	// username := c.PostForm("username")
	// password := c.PostForm("password")

	// if strings.Trim(username, " ") == "" || strings.Trim(password, " ") == "" {
	// 	c.JSON(http.StatusUnauthorized, gin.H{"error": "Parameters can't be empty"})
	// 	return
	// }
	// if username == "hello" && password == "itsme" {
	// session.Set("user", username) //In real world usage you'd set this to the users ID
	session.Set("user", "Luthfi")
	err := session.Save()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to generate session token"})
	} else {
		c.JSON(http.StatusOK, gin.H{"message": "Successfully authenticated user"})
	}
	// } else {
	// 	c.JSON(http.StatusUnauthorized, gin.H{"error": "Authentication failed"})
	// }
}
