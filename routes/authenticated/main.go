package authenticatedroutes

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func private(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	c.JSON(http.StatusOK, gin.H{"hello": user})
}

// Bootstrap the routes
func Bootstrap(router *gin.RouterGroup) {
	router.GET("/sayhi", sayhi)
	router.GET("/", private)
}
